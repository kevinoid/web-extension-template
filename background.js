/**
 * Background script.
 * https://developer.mozilla.org/docs/Mozilla/Add-ons/WebExtensions/Background_scripts
 *
 * @copyright Copyright 2022 Kevin Locke <kevin@kevinlocke.name>
 * @license MIT
 */

'use strict';

chrome.runtime.onInstalled.addListener((details) => {
  // Called when:
  // - The extension is first installed.
  // - The extension is updated to a new version.
  // - The browser is updated to a new version.
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  // Called in response to runtime.sendMessage() or tabs.sendMessage()
});

chrome.runtime.onStartup.addListener(() => {
  // Called when a profile that has this extension installed first starts up
  const manifest = chrome.runtime.getManifest();
  console.info(
    chrome.i18n.getMessage('extensionName'),
    manifest.version,
  );
});

chrome.runtime.onSuspend.addListener(() => {
  // Called when the event page is about to be unloaded
});
