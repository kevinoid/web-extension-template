WebExtension Project Template
=============================

[![Build Status](https://img.shields.io/gitlab/pipeline-status/kevinoid/web-extension-template.svg?branch=main&style=flat&label=build)](https://gitlab.com/kevinoid/web-extension-template/-/pipelines?ref=main)
[![Mozilla Add-ons](https://img.shields.io/amo/rating/web-extension-template?label=Firefox)](https://addons.mozilla.org/firefox/addon/web-extension-template/)
[![Chrome Web Store](https://img.shields.io/chrome-web-store/rating/ext-id?label=Chrome)](https://chrome.google.com/webstore/detail/ublock-origin/ext-id)

A WebExtension (browser extension/add-on) project template for
[Firefox](https://developer.mozilla.org/docs/Mozilla/Add-ons/WebExtensions),
[Chrome](https://developer.chrome.com/docs/extensions/), and compatible
browsers.


## Features

*


## Installation

This extension is available in browser extension directories for

* [Firefox](https://addons.mozilla.org/firefox/addon/web-extension-template/)
* [Chrome](https://chrome.google.com/webstore/detail/web-extension-template/ext-id)
* [Edge](https://microsoftedge.microsoft.com/addons/detail/web-extension-template/ext-id)
* [Opera](https://addons.opera.com/en/extensions/details/web-extension-template/)


## Contributing

Contributions are appreciated.  Contributors agree to abide by the [Contributor
Covenant Code of
Conduct](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html).
If this is your first time contributing to a Free and Open Source Software
project, consider reading [How to Contribute to Open
Source](https://opensource.guide/how-to-contribute/)
in the Open Source Guides.

If the desired change is large, complex, backwards-incompatible, can have
significantly differing implementations, or may not be in scope for this
project, opening an issue before writing the code can avoid frustration and
save a lot of time and effort.


## License

This project is available under the terms of the [MIT License](LICENSE.txt).
See the [summary at TLDRLegal](https://tldrlegal.com/license/mit-license).

The [template](https://gitlab.com/kevinoid/web-extension-template) upon which
this project is based is available under the terms of
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).
